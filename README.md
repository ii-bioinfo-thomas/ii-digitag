# DigiTAG – a RNA Sequencing Approach to Analyze Transcriptomes of Rare Cell Populations in Drosophila melanogaster



```
 ii-bioinfo@imp.ac.at
 IMP/IMBA bioinformatics core

 Cite: Lisa Landskron, Francois Bonnay, Thomas R. Burkard and Jürgen A. Knoblich: DigiTAG–a RNA Sequencing Approach to Analyze Transcriptomes of Rare Cell Populations in Drosophila melanogaster, 2020
```

## digiTag.pl

This script marks reads arising from duplication events. The index molecule (tag) must be stored in ID as following: `readID:multiplexBC:indexMolecule`

Usage:

```
samtools view -h input.bam | perl digiTag.pl | samtools view -bS - > output.bam
```