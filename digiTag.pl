#!/usr/bin/perl

# Mark digiTag duplication events
# Author: Thomas R Burkard, IMP/IMBA bioinformatics core
# Usage: samtools view -h input.bam | perl digiTag.pl | samtools view -bS - > output.bam


use strict;

sub main
{

    my $pos = -1;
    my $chr = "";
    my $tag = {};
    my $seq = {};

    while (my $line=<>)
    {
	#Header
	if ($line =~ /^@/)
	{
	    print $line;
	    next;
	}
	
	my @cell = split "\t", $line;
	
	if ($chr ne $cell[2] || $pos != $cell[3])
	{
	    if ( $chr ne "" )
	    {
		printSAM($tag, $seq);
		$tag = {};
		$seq = {};
	    }
	    $pos = $cell[3];
	    $chr = $cell[2];
	}

	my $t = $cell[0];
	$t =~ s/.*://;

	#each line by average PHRED
	$seq->{$t}->{&averageQual($cell[10])}->{$cell[0]} = \@cell;

	#tag count
	$tag->{$t}++;

    }

    printSAM($tag, $seq);
}

sub printSAM
{
    my ($tag, $seq) = @_;

    my $tagCnt = {};
    foreach my $t (keys %$tag)
    {
	push @{$tagCnt->{$tag->{$t}}}, $t;
    }

    my @tagPool;
    foreach my $cnt (sort { $b <=> $a } keys %$tagCnt)
    {
	#if several tags with the same count, sort alphanumerically
	foreach my $t (sort @{$tagCnt->{$cnt}})
	{
	    my $mm=2;
	    if (@tagPool)
	    {
		$mm=&numMM(\@tagPool, $t)
	    }

	    #if more than 1 MM to pool => add
	    push @tagPool, $t if ($mm > 1);
	
	    my $first = "TRUE";
	    #sort by average PHRED of tag => highest is not duplicate
	    foreach my $qual (sort {$b <=> $a} keys %{$seq->{$t}})
	    {
		#if several with same PHRED => sort alphanumerically
		foreach my $id (sort keys %{$seq->{$t}->{$qual}})
		{
		    if ( $first eq "TRUE" && $mm > 1 )
		    {
			#true read
			print join "\t", @{$seq->{$t}->{$qual}->{$id}};
			$first = "FALSE";
		    } else {
			#mark duplicate
			my @cell = @{$seq->{$t}->{$qual}->{$id}};
			$cell[1] = $cell[1] ^ 0x0400 ;
			print join "\t", @cell;
		    }
		}
	    }
	}
    }
}

sub numMM
{
    my ($tagPool, $tag) = @_;
    my @tag=split('',$tag);

    foreach my $tp (@$tagPool)
    {
	my @tp=split('',$tp);
	
	my $mm=0;
	foreach my $i (0 .. $#tp)
	{
	    $mm++ if ($tp[$i] ne $tag[$i]);
	}
	
	return $mm if ($mm < 2);
    }

    return 2;
}

sub averageQual
{

    my ($quality) = @_;
    my @qual=split('',$quality);

    my $averageScore = 0;
    foreach(@qual)
    {
	$averageScore+=ord($_)-33;
    }
    $averageScore=$averageScore/scalar(@qual);

    return($averageScore);
}

&main();
